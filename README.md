# Serverless Todo
A simple Task List application using AWS Lambda and Serverless framework as part of `Develop and Deploy Serverless Apps` Udacity course. 

# Run
GUI: `npm run start`

#### Features
1) Users to create/remove/update/get task list items.
2) A user needs to authenticate in order to use an application
3) S3 picture (re)upload
4) DynamoDB (and DynamoDB streams)
5) Middy for event and response validation
6) Fetch a certificate from Auth0 instead of hard coding it in the custom authorizer
