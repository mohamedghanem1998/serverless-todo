//https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/javascript_code_examples.html

import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import AWSXRay from 'aws-xray-sdk';

const dynamoDBClient = new DynamoDBClient({});

const dynamoDBClientXRay = AWSXRay.captureAWSv3Client(dynamoDBClient);

export default dynamoDBClientXRay;
