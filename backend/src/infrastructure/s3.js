//https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/javascript_code_examples.html

import { S3Client } from '@aws-sdk/client-s3';
import AWSXRay from 'aws-xray-sdk';

const s3Client = new S3Client({});

const s3ClientXray = AWSXRay.captureAWSv3Client(s3Client);

export default s3ClientXray;
