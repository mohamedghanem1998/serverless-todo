import s3ClientXray from '../../infrastructure/s3.js';
import TodoModel from '../../models/todo.js';
import {
  eventSchema,
  responseSchema,
} from '../../specifications/paths/generateUploadUrl.js';
import middlewareWrapper from '../../utils/middlewareWrapper.js';
import { PutObjectCommand } from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';

async function getUploadURL(event) {
  const todoId = event.pathParameters.todoId;
  const user = JSON.parse(event.requestContext.authorizer.user);

  const key = { userId: user.sub, todoId };

  const params = {
    Bucket: process.env.TODOS_BUCKET,
    Key: `${todoId}.jpeg`,
  };

  const command = new PutObjectCommand(params);

  const signedUrl = await getSignedUrl(s3ClientXray, command, {
    expiresIn: 3600,
  });
  const attachmentUrl = `https://${process.env.TODOS_BUCKET}.s3.amazonaws.com/${params.Key}`;

  await TodoModel.updateTodo(key, { attachmentUrl });

  return {
    statusCode: 201,
    body: { uploadUrl: signedUrl },
  };
}

export const handler = middlewareWrapper({
  handler: getUploadURL,
  parseJSON: false,
  findTodo: true,
  eventSchema,
  responseSchema,
});
