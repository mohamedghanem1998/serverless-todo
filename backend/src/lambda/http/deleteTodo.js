import middlewareWrapper from '../../utils/middlewareWrapper.js';
import { eventSchema } from '../../specifications/paths/deleteTodo.js';
import TodoModel from '../../models/todo.js';
import s3ClientXray from '../../infrastructure/s3.js';
import { DeleteObjectCommand } from '@aws-sdk/client-s3';
import { createLogger } from '../../utils/logger.mjs';

const deleteTodo = async (event) => {
  const logger = createLogger('deleteTodo');

  const todoId = event.pathParameters.todoId;
  const user = JSON.parse(event.requestContext.authorizer.user); // retrieved from authorizer
  const todo = JSON.parse(event.todo); // retrieved from checkTodoExists middleware

  const key = { userId: user.sub, todoId };

  await TodoModel.deleteTodo(key);

  if (todo.attachmentUrl) {
    logger.info('deleting todo image');
    const command = new DeleteObjectCommand({
      Bucket: process.env.TODOS_BUCKET,
      Key: `${todoId}.jpeg`,
    });
    await s3ClientXray.send(command);
  }

  return {
    statusCode: 200,
    body: {},
  };
};

export const handler = middlewareWrapper({
  handler: deleteTodo,
  eventSchema,
  parseJSON: false,
  findTodo: true,
});
