const logDynamoStreamEvents = async (event) => {
  for (const record of event.Records) {
    console.log('Processing record', JSON.stringify(record));
  }
};

export const handler = logDynamoStreamEvents;
