import middy from '@middy/core';
import cors from '@middy/http-cors';
import httpErrorHandler from '@middy/http-error-handler';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpResponseSerializer from '@middy/http-response-serializer';
import validator from '@middy/validator';
import checkTodoExists from './middleware/checkTodoExists.js';

const middleware = (config) => {
  const { handler, eventSchema, responseSchema, parseJSON, findTodo } = config;

  /* !!!!! IMPORTANT !!!!! Middleware follow onion-like sequence (see middy documentation) */
  /* Sequence influences the flow. e.g: httpJsonBodyParser() before hook must run before validator before hook (for eventSchema)  */

  /* !!!!! This sequence must be enforce*/

  /* BEFORE Request middlwares sequence:
        1- httpJsonBodyParser()
        2- validator({eventSchema})
    */

  /* AFTER Reponse middlwares sequence (hence afterMiddleware is in reverse order):
        1- validator({responseSchema})
        2- httpResponseSerializer()
        3- cors()
    */

  /* This enforces that validator({eventSchema}) is never before httpJsonBodyParser()
       AND serialization happens after lambda response as the validator cannot process serialized data.

       TIP: order middlewares with an after hook in the order of importance.
    */

  const beforeMiddleware = [];

  if (parseJSON || parseJSON === undefined) {
    beforeMiddleware.push(httpJsonBodyParser());
  }

  if (eventSchema) {
    beforeMiddleware.push(validator({ eventSchema }));
  }

  if (findTodo) {
    beforeMiddleware.push(checkTodoExists());
  }

  const afterMiddleware = [];

  afterMiddleware.push(cors());
  afterMiddleware.push(
    httpResponseSerializer({
      serializers: [
        {
          regex: /^application\/xml$/,
          serializer: ({ body }) => `<message>${body}</message>`,
        },
        {
          regex: /^application\/json$/,
          serializer: ({ body }) => JSON.stringify(body),
        },
        {
          regex: /^text\/plain$/,
          serializer: ({ body }) => body,
        },
      ],
      defaultContentType: 'application/json',
    })
  );

  if (responseSchema) {
    afterMiddleware.push(validator({ responseSchema }));
  }

  return middy()
    .use(beforeMiddleware)
    .use(afterMiddleware)
    .use(httpErrorHandler())
    .handler(handler);
};
export default middleware;
